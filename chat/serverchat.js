const express = require ('express');
const app = express ();


app.use(express.static('Public'));
app.get('/', function(req, res){
	res.sendFile('/index.html')
})


const server = app.listen(8000, function(){
	console.log('Hakuna Matata')
})

const socket = require ('socket.io');
const io = socket(server);

io.on('connection', (socket)=>{

	console.log('a user connected aka Relou Incoming');

	socket.on('disconnect', function(){
    console.log('user disconnected aka wow good riddance it was time for him to take a hint');
  });
  
   // socket.on('stringOfWords', function(data_msg){ // as soon as user emit msg/ data
   socket.on('stringOfWords', function(recup_stringOfWords){ 

    	io.sockets.emit('stringOfWords',recup_stringOfWords) // send data back to all sockets
       ;
  	});

});